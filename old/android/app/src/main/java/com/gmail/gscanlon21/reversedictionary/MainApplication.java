package com.gmail.gscanlon21.reversedictionary;

import android.app.Application;
import com.facebook.react.BuildConfig;
import com.facebook.react.ReactApplication;

import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import io.invertase.firebase.admob.RNFirebaseAdMobPackage; //Firebase AdMob
import io.invertase.firebase.perf.RNFirebasePerformancePackage; // Firebase Messaging

import com.oblador.vectoricons.VectorIconsPackage;
import fm.indiecast.rnaudiostreamer.RNAudioStreamerPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
          new RNFirebasePackage(),
          //new RealmReactPackage(),
          new RNFirebaseAdMobPackage(),
          //new RealmReactPackage(),
          new RNFirebaseAnalyticsPackage(),
          //new RNFirebaseCrashPackage(),
          new VectorIconsPackage(),
          new RNAudioStreamerPackage(),
          new RNFirebasePerformancePackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
