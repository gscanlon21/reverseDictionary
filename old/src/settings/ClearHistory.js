
import {
  ToastAndroid,
  Platform,
  AsyncStorage } from 'react-native'
/* Dev - settings */
import { loadHistory } from './../settings/Settings'

async function clearHistory () {
  // Clears the user's history
  // Clears history by setting it to an empty list
  let hist = await loadHistory()
  hist = hist || []
  for (let i = 0; i < hist.length; i++) {
    if (await AsyncStorage.getItem(hist[0]) !== null) {
      await AsyncStorage.removeItem(hist[0])
    }
  }

  await AsyncStorage.removeItem('history')
  if (Platform.OS === 'android') {
    ToastAndroid.show(('History cleared successfully'), ToastAndroid.SHORT)
  }
}

async function clearHistorySilent () {
  // Called when user disables history so when the user
  // ... re-enables history they see a fresh, blank history list
  // Clears history by setting it to an empty list
  let hist = await loadHistory()
  hist = hist || []
  for (let i = 0; i < hist.length; i++) {
    if (await AsyncStorage.getItem(hist[0]) !== null) {
      await AsyncStorage.removeItem(hist[0])
    }
  }

  await AsyncStorage.removeItem('history')
}

export { clearHistory, clearHistorySilent }
