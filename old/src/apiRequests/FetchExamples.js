
// import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import secretAPIKey from './../secrets/secrets'
import handleErrors from './../utilities/HandleFetchErrors'

function fetchExamples (word, hideEmptyPages) {
  let uri = 'http://api.wordnik.com:80/v4/word.json/' + word + '/examples?includeDuplicates=false&useCanonical=true&skip=0&limit=9&api_key=' + secretAPIKey + ''
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      var examp = res.examples
      if (examp) {
        if (examp.length < 1 && hideEmptyPages) {
          examp = []
        }
      } else if (hideEmptyPages) {
        examp = []
      }
      resolve(examp)
    }).catch(error => {
      console.log(error)
      // if (Platform.OS === 'android') {
      //  ToastAndroid.show(('Part of the API may be down'), ToastAndroid.SHORT)
      // }
      reject([])
    })
  })
}

export default fetchExamples
