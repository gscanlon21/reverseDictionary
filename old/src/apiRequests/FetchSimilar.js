
// import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import handleErrors from './../utilities/HandleFetchErrors'

function fetchSimilar (word, hideEmptyPages, max) {
  // Fetches the WOTD
  let uri = 'https://api.datamuse.com/words?rel_trg=' + word + '&md=d&max=' + max + ''
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      if (res.length < 1 && hideEmptyPages) {
        var similar = []
      } else {
        similar = res
      }
      resolve(similar)
    }).catch(error => {
      console.log(error)
      // if (Platform.OS === 'android') {
      //  ToastAndroid.show(('Part of the API may be down'), ToastAndroid.SHORT)
      // }
      reject([])
    })
  })
}

export default fetchSimilar
