
import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import secretAPIKey from './../secrets/secrets'
import handleErrors from './../utilities/HandleFetchErrors'

function fetchAudioUri (word) {
  let uri = 'http://api.wordnik.com:80/v4/word.json/' + word + '/audio?useCanonical=true&limit=2&api_key=' + secretAPIKey + ''
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      if (res && res[0]) {
        let audioUri = res[0].fileUrl
        resolve(audioUri)
      } else {
        resolve('')
      }
    }).catch(error => {
      console.log(error)
      if (Platform.OS === 'android') {
        ToastAndroid.show(('Unable to connect'), ToastAndroid.SHORT)
      }
      reject('')
    })
  })
}

export default fetchAudioUri
