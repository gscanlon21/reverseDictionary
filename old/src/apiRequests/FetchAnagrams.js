
// import fetch from 'unfetch'
import handleErrors from './../utilities/HandleFetchErrors'

function fetchAnagrams (word, hideEmptyPages) {
  let uri = 'http://www.anagramica.com/all/:' + word + ''
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      let anagrams = res.all.filter(function (item) {
        return item.length > 2
      })
      if (anagrams) {
        if (anagrams.length < 1 && hideEmptyPages) {
          anagrams = []
        }
      } else if (hideEmptyPages) {
        anagrams = []
      }
      resolve(anagrams)
    }).catch(error => {
      // Anagramica always throws an error for long words
      reject([])
    })
  })
}

export default fetchAnagrams
