
import { StyleSheet } from 'react-native'

const spacingStyles = StyleSheet.create({
  paddingHorizontal12: {
    paddingHorizontal: 12
  },
  paddingHorizontal0: {
    paddingHorizontal: 0
  },
  paddingLeft12: {
    paddingLeft: 12
  },
  paddingLeft6: {
    paddingLeft: 6
  },
  paddingLeft24: {
    paddingLeft: 24
  },
  paddingTop12: {
    paddingTop: 12
  },
  paddingTop6: {
    paddingTop: 6
  },
  paddingVertical6: {
    paddingVertical: 6
  },
  paddingBottom6: {
    paddingBottom: 6
  },
  paddingBottom12: {
    paddingBottom: 12
  },
  marginBottom6: {
    marginBottom: 6
  },
  marginBottom12: {
    marginBottom: 12
  },
  marginBottom24: {
    marginBottom: 24
  },
  marginLeft08: {
    marginLeft: -8
  },
  marginRight08: {
    marginRight: -8
  }
})

export default spacingStyles
