
import { StyleSheet } from 'react-native'

const settingsPageStyles = StyleSheet.create({
  // StyleSheet
  iconStyle: {
    marginLeft: 15,
    marginRight: 0,
    alignSelf: 'center',
    width: 26,
    height: 26,
    justifyContent: 'center'
  }
})

export default settingsPageStyles
