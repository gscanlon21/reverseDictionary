
import { StyleSheet } from 'react-native'

/*
* Effort to eventually remove duplicate and
* inconsistant text-styles across components
*/

const textStyles = StyleSheet.create({
  // StyleSheet
  subTitleText: {
    color: '#000', // Black
    fontSize: 16,
    paddingBottom: 6
  },
  titleText: {
    color: '#000', // Black
    fontSize: 18,
    paddingBottom: 6
  },
  text: {
    color: '#000', // Black
    fontSize: 14,
    paddingBottom: 6
  },
  textBold: {
    fontWeight: 'bold'
  },
  fontWeight700: {
    fontWeight: '700'
  },
  textItalic: {
    fontStyle: 'italic'
  },
  link: {
    color: 'blue'
  },
  settingsTitleText: {
    color: '#009688', // Green-ish
    marginBottom: 0,
    fontWeight: '500'
  }
})

export default textStyles
