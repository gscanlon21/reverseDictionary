
import { StyleSheet } from 'react-native'

const aboutPageStyles = StyleSheet.create({
  // StyleSheet
  image: {
    marginLeft: 18,
    width: 128,
    height: 24
  },
  link: {
    color: 'blue'
  }
})

export default aboutPageStyles
