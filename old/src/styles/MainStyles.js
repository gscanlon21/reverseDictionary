
import { StyleSheet } from 'react-native'

const mainStyles = StyleSheet.create({
  wrapper: {
    justifyContent: 'flex-start',
    flex: 1,
    alignSelf: 'stretch',
    paddingHorizontal: 6
  },
  iconLeft: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    alignSelf: 'center',
    width: 28,
    height: 28,
    justifyContent: 'center'
  },
  iconRight: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    alignSelf: 'center',
    width: 28,
    height: 28,
    justifyContent: 'center'
  },
  iconHome: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    alignSelf: 'center',
    width: 32,
    height: 32,
    justifyContent: 'center'
  },
  touchableLeft: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    alignSelf: 'center',
    width: 60,
    height: 60,
    justifyContent: 'center'
  },
  touchableRight: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    alignSelf: 'center',
    width: 60,
    height: 60,
    justifyContent: 'center'
  },
  list: {
    alignSelf: 'stretch',
    flex: 1
  },
  listWrapper: {
    flex: 1
  },
  bannerAd: {
    position: 'relative',
    bottom: 0,
    paddingHorizontal: -6,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default mainStyles
