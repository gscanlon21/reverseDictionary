
import React from 'react'
import {
  View,
  Text,
  FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'
import copyToClipboard from './../utilities/CopyToClipboard'
import { onPressItemSearchPage } from './../navigation/OnPressItem'
import { historyHeader } from './Headers'
import footer from './Footers'

function historyList (history, listOverride, height, width, settings, similar) {
  // History's a little wonky now
  // Renders previous search history

  return (
    <FlatList
      style={listOverride}
      data={history}
      renderItem={({ item }) => (
        <ListItem
          onPressRightIcon={() => onPressItemSearchPage(
            item,
            height,
            width,
            settings,
            history,
            similar
          )}
          titleNumberOfLines={1}
          onLongPress={() => copyToClipboard(item)}
          onPress={() => onPressItemSearchPage(
            item,
            height,
            width,
            settings,
            history,
            similar
          )}
          title={item}
        />
      )}
      keyExtractor={(item, index) => index}
      initialNumToRender={7}
      ListFooterComponent={() => footer(settings)}
      ListHeaderComponent={historyHeader} />
  )
}

export default historyList
