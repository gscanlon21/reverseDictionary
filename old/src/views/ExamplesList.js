
import React from 'react'
import {
  View,
  FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'
import copyToClipboard from './../utilities/CopyToClipboard'
import listEmptyView from './ListEmptyView'
import footer from './Footers'
import styleExamples from './../utilities/StyleExamples'
import { exampleHeader } from './Headers'
import styles from './../styles/StyleWrapper'

function examplesList (examples, overrides, word, settings) {
  // Code to display the list of example sentences
  let lowercaseWord = word.toLowerCase()
  let listOverride = overrides.listOverride
  let widthOverride = overrides.widthOverride

  return (
    <View style={widthOverride}>
      <FlatList
        style={listOverride}
        data={examples}
        renderItem={({ item }) => (
          <ListItem
            titleNumberOfLines={12}
            onLongPress={() => copyToClipboard(item.text.replace(/\*|_/g, ''))}
            title={styleExamples(item.text, lowercaseWord)}
            hideChevron />
        )}
        initialNumToRender={7}
        enableEmptySections
        onEndReachedThreshold={0.1}
        ListFooterComponent={() => footer(settings)}
        ListHeaderComponent={exampleHeader(word)}
        ListEmptyComponent={listEmptyView}
        keyExtractor={(item, index) => index} />
    </View>
  )
}

export default examplesList
