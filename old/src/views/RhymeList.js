
import React from 'react'
import {
  View,
  FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'
import copyToClipboard from './../utilities/CopyToClipboard'
import { onPressItemSearchResultPage } from './../navigation/OnPressItem'
import listEmptyView from './ListEmptyView'
import footer from './Footers'
import { rhymeHeader } from './Headers'

function rhymeList (rhymes, overrides, settings, height, width, word) {
  let listOverride = overrides.listOverride
  let widthOverride = overrides.widthOverride
  let request = console.log('not working')

  return (
    <View style={widthOverride}>
      <FlatList
        ref={(ref) => { this.flatListRhymesRef = ref }}
        style={listOverride}
        data={rhymes}
        renderItem={({ item }) => (
          <ListItem
            onPress={() => onPressItemSearchResultPage(
              item.word || item,
              this.callbacks.settings,
              this.callbacks.height,
              this.callbacks.width
            )}
            onPressRightIcon={() => onPressItemSearchResultPage(
              item.word || item,
              this.callbacks.settings,
              this.callbacks.height,
              this.callbacks.width
            )}
            onLongPress={() => copyToClipboard(item.word || item)}
            titleNumberOfLines={1}
            title={item.word || item} />
        )}
        initialNumToRender={14}
        onEndReached={request}
        onEndReachedThreshold={0.1}
        enableEmptySections
        ListFooterComponent={footer}
        ListHeaderComponent={rhymeHeader(word)}
        ListEmptyComponent={listEmptyView}
        keyExtractor={(item, index) => index} />
    </View>
  )
}

export default rhymeList
