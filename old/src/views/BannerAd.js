
import React from 'react'
import {
  View,
  StyleSheet } from 'react-native'
import styles from './../styles/StyleWrapper'
import { secretAdModBannerKey } from './../secrets/secrets'
// Below import currently not working with jest for SearchPageTest.js
// ...TypeError: Cannot read property 'admob' of undefined
import { Banner, adRequest } from './../constants/Ads'

function renderBannerAd (width) {
  // Renders a banner ad at the bottom of the screen
  let bannerOverride = StyleSheet.flatten([
    styles.banner,
    {paddingLeft: (width - 332) / 2}
  ])

  // TEST_BANNER_KEY // ca-app-pub-3940256099942544/6300978111
  return (
    <View style={bannerOverride}>
      <Banner
        size={'BANNER'}
        unitId={secretAdModBannerKey}
        request={adRequest.build()}
      />
    </View>
  )
}

export default renderBannerAd
