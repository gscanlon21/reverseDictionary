
import React from 'react'
import { View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'

function loading () {
  // Loading overlay when user makes API request
  return (
    <View><Spinner visible textContent={'loading...'} textStyle={{color: '#FFF'}} /></View>
  )
}

export default loading
