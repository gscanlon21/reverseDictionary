import { Actions } from 'react-native-router-flux'
import addToHistory from './../utilities/History'

async function onPressWOTDItem (word, height, width, settings) {
  // Can move statics to seperate file
  // User presses on the WOTD button
  word = (JSON.stringify(word)).slice(1, -1)

  addToHistory(
    word,
    height,
    settings
  )

  Actions.searchResult({
    word: word,
    title: word,
    height: height,
    width: width,
    settings: settings
  })
};

export default onPressWOTDItem
