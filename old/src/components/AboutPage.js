
import React, { Component } from 'react'
import {
  View,
  Text,
  Keyboard,
  BackHandler,
  Image } from 'react-native'
// Dev - styles
import styles from './../styles/StyleWrapper'
// Dev - utilitis
import backAndroid from './../utilities/BackAndroid'
// Dev - constants
import {
  datamuseAPILink,
  anagramicaLink,
  sourceCodeLink,
  wordnikAPILink } from './../constants/ExternalLinks'

const aboutText = [styles.text, styles.paddingHorizontal12]

class About extends Component {
  componentDidMount () {
    // After first frame of component is rendered
    Keyboard.dismiss()
    BackHandler.addEventListener('hardwareBackPress', backAndroid) // Listen for the hardware back button on Android to be pressed
  }

  componentWillUnmount () {
    BackHandler.removeEventListener('hardwareBackPress', backAndroid) // Remove listener
  }

  render () {
    return (
      <View style={styles.wrapper}>
        <Text style={[styles.titleText, styles.borderBottom, styles.marginBottom6]}>Reverse Dictionary v1.5.6</Text>
        <Text style={aboutText}>Made by Graham Scanlon (gscanlon21@gmail.com)</Text>
        <Text style={aboutText}>
          Source code and support availble at
          <Text style={aboutText} onPress={sourceCodeLink}> Bitbucket
          </Text>
        </Text>
        <Text style={aboutText}>
          Receives data from the
          <Text style={styles.link} onPress={datamuseAPILink}> datamuse API</Text>, the
          <Text style={styles.link} onPress={wordnikAPILink}> wordnik API </Text>
          and the
          <Text style={styles.link} onPress={anagramicaLink}> anagramica API</Text>
        </Text>
        <Image resizeMode={Image.resizeMode.contain} source={{uri: 'wordnik'}} style={[styles.image, styles.marginBottom12]} />

        <Text style={[styles.subTitleText, styles.borderBottom, styles.marginBottom6]}>Help</Text>
        <Text style={aboutText}>Long press an item to copy</Text>
        <Text style={aboutText}>Turn up media volume before listening to the audio pronunciation</Text>
        <Text style={aboutText}>Use <Text style={styles.textBold}>*</Text> as the wildcard character in searches (sa. <Text style={styles.textBold}>water*on</Text> or <Text style={styles.textBold}>* pie</Text>)</Text>
      </View>
    )
  }
}

export default About
