
import React, { Component } from 'react'
import {
  Text,
  View,
  Slider,
  Keyboard,
  BackHandler,
  AsyncStorage } from 'react-native'
// Libraries
import SettingsList from 'react-native-settings-list'
import Icon from 'react-native-vector-icons/MaterialIcons'
// Dev - styles
import styles from './../styles/StyleWrapper'
// Dev - settings
import {
  anagramsSwitched,
  historySwitched,
  sourceSwitched,
  rhymesSwitched,
  antonymsSwitched,
  synonymsSwitched,
  similarSwitched,
  pagingSwitched,
  examplesSwitched,
  offlineChacheSwitched,
  wwfSwitched,
  hideEmptyPages,
  adsSwitched } from './../settings/Switches'
import {
  clearHistory,
  clearHistorySilent } from './../settings/ClearHistory'
// Dev - utilities
import renderIf from './../utilities/renderIf'
import backAndroid from './../utilities/BackAndroid'

class Settings extends Component {
  constructor (props) {
    super(props)
    this.state = {
      settings: {}
    }
    this.callbacks = {
      historyLength: 25
    }
  }

  async componentDidMount () {
    // componentDidMount is called twice in debug mode, causing settings to
    // ...break, it's stable in production.
    // After first frame of component is rendered.
    BackHandler.addEventListener('hardwareBackPress', backAndroid) // Listen for the hardware back button on Android to be pressed
    Keyboard.dismiss()
    let settings = JSON.parse(await AsyncStorage.getItem('settings'))
    // TODO: Remove setState from componentDidMount
    this.setState({settings: settings})
  }

  componentWillUnmount () {
    // Remove listener
    BackHandler.removeEventListener('hardwareBackPress', backAndroid)
  }

  async updateState (settings) {
    this.setState({settings: settings})
    await AsyncStorage.setItem('settings', JSON.stringify(settings))
  }

  render () {
    let settings = this.state.settings
    return (
      <View>
        <SettingsList borderColor='#d6d5d9' defaultItemSize={70} defaultTitleStyle={styles.titleTitleText}>
        <SettingsList.Item
          hasNavArrow={false}
          title='General'
          titleStyle={styles.settingsTitleText}
          itemWidth={50}
          borderHide={'Both'}
          />
        <SettingsList.Item
          icon={
            <View style={styles.iconStyle}>
              <Icon name='view-array' size={26} />
            </View>}
          title='Enable Paging'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.pagingEnabled}
          switchOnValueChange={() => this.updateState(pagingSwitched(settings))}
          />
        <SettingsList.Item
          icon={
              renderIf(!settings.hideEmptyPages)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility' size={26} />
                </View>
              ) ||
              renderIf(settings.hideEmptyPages)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility-off' size={26} />
                </View>
              )}
          title='Hide empty pages'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.hideEmptyPages}
          switchOnValueChange={() => this.updateState(hideEmptyPages(settings))}
          />
        <SettingsList.Item
          icon={
                renderIf(settings.ads)(
                  <View style={styles.iconStyle}>
                    <Icon name='visibility' size={26} />
                  </View>
                ) ||
                renderIf(!settings.ads)(
                  <View style={styles.iconStyle}>
                    <Icon name='visibility-off' size={26} />
                  </View>
                )}
          borderHide={'Both'}
          title='Enable Ads'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.ads}
          switchOnValueChange={() => this.updateState(adsSwitched(settings))}
          titleInfo={'If disabled,\nusage statistics\nwill be sent instead'}
            />
        <SettingsList.Item
          hasNavArrow={false}
          title='History'
          titleStyle={styles.settingsTitleText}
          itemWidth={50}
          borderHide={'Bottom'}
          />
        <SettingsList.Item
          icon={
            <View style={styles.iconStyle}>
              <Icon name='history' size={26} />
            </View>
            }
          hasNavArrow={false}
          title='Clear History'
          onPress={() => clearHistory()}
          />
        <SettingsList.Item
          icon={
              renderIf(settings.historyDisabled)(
                <View style={styles.iconStyle}>
                  <Icon name='layers-clear' size={26} />
                </View>
              ) ||
              renderIf(!settings.historyDisabled)(
                <View style={styles.iconStyle}>
                  <Icon name='layers' size={26} />
                </View>
              )}
          borderHide={'Both'}
          title='Disable History'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.historyDisabled}
          switchOnValueChange={() => (this.updateState(historySwitched(settings)) && clearHistorySilent())}
          />
        <SettingsList.Item
          hasNavArrow={false}
          title='Data Source'
          titleStyle={styles.settingsTitleText}
          itemWidth={50}
          borderHide={'Bottom'}
          />
        <SettingsList.Item
          icon={
            <View style={styles.iconStyle}>
              <Icon name='library-books' size={26} />
            </View>}
          borderHide={'Both'}
          title='Use Wordnik API'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.sourceIsWordnik}
          switchOnValueChange={() => this.updateState(sourceSwitched(settings))}
          titleInfo={'Default is the\ndatamuse API'}
          />
        <SettingsList.Item
          hasNavArrow={false}
          title='Visible Pages'
          titleStyle={styles.settingsTitleText}
          itemWidth={50}
          borderHide={'Bottom'}
          />
        <SettingsList.Item
          icon={
              renderIf(settings.antonymsVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility' size={26} />
                </View>
              ) ||
              renderIf(!settings.antonymsVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility-off' size={26} />
                </View>
              )}
          title='Antonyms Visible'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.antonymsVisible}
          switchOnValueChange={() => this.updateState(antonymsSwitched(settings))}
          />
        <SettingsList.Item
          icon={
              renderIf(settings.rhymesVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility' size={26} />
                </View>
              ) ||
              renderIf(!settings.rhymesVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility-off' size={26} />
                </View>
              )}
          title='Rhymes Visible'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.rhymesVisible}
          switchOnValueChange={() => this.updateState(rhymesSwitched(settings))}
          />
        <SettingsList.Item
          icon={
              renderIf(settings.synonymsVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility' size={26} />
                </View>
              ) ||
              renderIf(!settings.synonymsVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility-off' size={26} />
                </View>
              )}
          title='Synonyms Visible'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.synonymsVisible}
          switchOnValueChange={() => this.updateState(synonymsSwitched(settings))}
          />
        <SettingsList.Item
          icon={
              renderIf(settings.similarVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility' size={26} />
                </View>
              ) ||
              renderIf(!settings.similarVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility-off' size={26} />
                </View>
              )}
          borderHide={'Both'}
          title='Similar words visible'
          hasNavArrow={false}
          hasSwitch
          switchState={settings.similarVisible}
          switchOnValueChange={() => this.updateState(similarSwitched(settings))}
          />
        <SettingsList.Item
          hasNavArrow={false}
          title='Visible Pages - Uses Wordnik API'
          titleStyle={styles.settingsTitleText}
          itemWidth={50}
          borderHide={'Bottom'}
          />
        <SettingsList.Item
          icon={
              renderIf(settings.examplesVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility' size={26} />
                </View>
              ) ||
              renderIf(!settings.examplesVisible)(
                <View style={styles.iconStyle}>
                  <Icon name='visibility-off' size={26} />
                </View>
              )}
          title='Example sentences visible'
          hasNavArrow={false}
          hasSwitch
          borderHide={'Both'}
          switchState={settings.examplesVisible}
          switchOnValueChange={() => this.updateState(examplesSwitched(settings))}
          />
        <SettingsList.Item
          hasNavArrow={false}
          title='Visible Pages - Uses Anagramica API'
          titleStyle={styles.settingsTitleText}
          itemWidth={50}
          borderHide={'Bottom'}
            />
        <SettingsList.Item
          icon={
                renderIf(settings.anagramsVisible)(
                  <View style={styles.iconStyle}>
                    <Icon name='visibility' size={26} />
                  </View>
                ) ||
                renderIf(!settings.anagramsVisible)(
                  <View style={styles.iconStyle}>
                    <Icon name='visibility-off' size={26} />
                  </View>
                )}
          title='Anagrams visible'
          hasNavArrow={false}
          hasSwitch
          borderHide={'Both'}
          switchState={settings.anagramsVisible}
          switchOnValueChange={() => this.updateState(anagramsSwitched(settings))}
            />
        <SettingsList.Item
          hasNavArrow={false}
          title='Word Score'
          titleStyle={styles.settingsTitleText}
          itemWidth={50}
          borderHide={'Bottom'}
            />
        <SettingsList.Item
          icon={
                renderIf(settings.wwfVisible)(
                  <View style={styles.iconStyle}>
                    <Icon name='visibility' size={26} />
                  </View>
                ) ||
                renderIf(!settings.wwfVisible)(
                  <View style={styles.iconStyle}>
                    <Icon name='visibility-off' size={26} />
                  </View>
                )}
          title='Use Words with Friends Scoring'
          titleInfo={'Defaults to\nScrabble®'}
          hasNavArrow={false}
          hasSwitch
          borderHide={'Both'}
          switchState={settings.wwfVisible}
          switchOnValueChange={() => this.updateState(wwfSwitched(settings))}
            />

        <SettingsList.Item
          hasNavArrow={false}
          title='Offline Access'
          titleStyle={styles.settingsTitleText}
          itemWidth={50}
          borderHide={'Bottom'}
            />
        <SettingsList.Item
          icon={
                renderIf(settings.offlineCache)(
                  <View style={styles.iconStyle}>
                    <Icon name='visibility' size={26} />
                  </View>
                ) ||
                renderIf(!settings.offlineCache)(
                  <View style={styles.iconStyle}>
                    <Icon name='visibility-off' size={26} />
                  </View>
                )}
          title='Cache Data'
          titleInfo={'For offline access\nto your history'}
          hasNavArrow={false}
          hasSwitch
          switchState={settings.offlineCache}
          switchOnValueChange={() => this.updateState(offlineChacheSwitched(settings))}
            />
      </SettingsList></View>
    )
  }
}

export default Settings
