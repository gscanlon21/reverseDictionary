
import React, { Component } from 'react'
import {
  View,
  FlatList,
  ScrollView,
  InteractionManager,
  AsyncStorage,
  BackHandler } from 'react-native'

/* Libraries */
import { ListItem } from 'react-native-elements'
import renderIf from './../utilities/renderIf'

/* Dev - styles */
import styles from './../styles/StyleWrapper'
import { searchResultOverrides } from './../styles/Overrides'
      // (lessenWidth = false, height, width, pagingEnabled)

/* Dev - navigation */
import { onPressItemSearchResultPage } from './../navigation/OnPressItem'
      // (word, settings, height, width)

/* Dev - views */
import footer from './../views/Footers'
import listEmptyView from './../views/ListEmptyView'
import loading from './../views/Loading'
import anagramsList from './../views/AnagramsList'
      // (anagrams, overrides, settings, height, width, word)
import examplesList from './../views/ExamplesList'
      // (examples, overrides, word)
import definitionsList from './../views/DefinitionsList'
      // (definitions, overrides, word, audioUri)
import {
  rhymeHeader,
  similarHeader,
  synonymHeader,
  antonymHeader } from './../views/Headers'
      // (word) // (word) // (word) // (word)

/* Dev - utilities */
import checkNetwork from './../utilities/Checks'
import copyToClipboard from './../utilities/CopyToClipboard'
      // (word)
import backAndroid from './../utilities/BackAndroid'

/* Dev - apiRequests */
import fetchAudioUri from './../apiRequests/FetchAudioUri'
      // (word)
import fetchExamples from './../apiRequests/FetchExamples'
      // (word, hideEmptyPages)
import fetchAnagrams from './../apiRequests/FetchAnagrams'
      // (word, hideEmptyPages)
import fetchSynonyms from './../apiRequests/FetchSynonyms'
      // (word, hideEmptyPages, max)
import fetchAntonyms from './../apiRequests/FetchAntonyms'
      // (word, hideEmptyPages, max)
import fetchSimilar from './../apiRequests/FetchSimilar'
      // (word, hideEmptyPages, max)
import fetchRhymes from './../apiRequests/FetchRhymes'
      // (word, hideEmptyPages, max)
import fetchWordnik from './../apiRequests/FetchWordnik'
      // (word, hideEmptyPages, max)
import fetchDefintionsWordnik from './../apiRequests/FetchDefinitionsWordnik'
      // (word, loop)
import fetchDefintionsDatamuse from './../apiRequests/FetchDefinitionsDatamuse'
      // (word, loop)

class SearchResult extends Component {
  constructor (props) {
    super(props)
    this.callbacks = {
      settings: undefined,
      word: props.word,
      lowercaseWord: props.word.toLowerCase(),
      pagingEnabled: props.settings.pagingEnabled,
      audioURI: undefined,
      height: props.height,
      width: props.width,
      onePageOfData: Math.floor((props.height) / 45),
      score: 0
    }
    this.state = {
      definitions: [],
      synonyms: [],
      antonyms: [],
      rhymes: [],
      similar: [],
      anagrams: [],
      examples: []
    }
  }

  componentWillUnmount () {
    /* Remove listener when component is navigated away from */
    BackHandler.removeEventListener('hardwareBackPress', backAndroid)
  }

  async componentWillMount () {
    /* Before first frame of component is rendered */
    // Somehow settings gets converted to a promise when passed as a prop
    let settings = await this.props.settings
    this.callbacks.settings = settings
  }

  async componentDidMount () {
    /* After first frame of component is rendered */
    let cached = await AsyncStorage.getItem(this.callbacks.lowercaseWord)
    InteractionManager.runAfterInteractions(() => {
      if (cached === null) {
        this.makeRemoteRequest()
        checkNetwork()
      } else {
        this.getCachedData()
      }
    })
    // Listen for the hardware back button on Android to be pressed
    BackHandler.addEventListener('hardwareBackPress', backAndroid)
  }

  onLayout (event) {
    /* Redefines height and width if user has turned screen to landscape mode */
    setTimeout(() => {
      let {width, height} = event.nativeEvent.layout
      if (width > height) {
        this.callbacks.width = width / 1.8
        this.callbacks.pagingEnabled = false
      }
    }, 50)
    // Allow keyboard to close so it doesn't interfere with the measurements
  }

  async getCachedData () {
    let cachedData = JSON.parse(await AsyncStorage.getItem(this.callbacks.lowercaseWord))
    this.setState(cachedData)

    // Flashes scroll indicator to givs an indication of more horizontal content
    this.scrollViewRef.scrollTo({x: 0, y: 0, animated: true})
  }

  async makeRemoteRequest () {
    /* Initial API requests */
    let settings = this.callbacks.settings
    let page = 1
    let loop = 1
    let word = this.callbacks.lowercaseWord
    let max = Math.floor((this.callbacks.height / 45) * page)
    let hideEmptyPages = settings.hideEmptyPages

    if (settings.sourceIsWordnik) {
      // Source is wordnik
      let definitions = await fetchDefintionsWordnik(word, loop).catch(error => (definitions = error))
      this.setState({ definitions: definitions })
      var definitionData = { definitions: definitions }

      var wordnikData = await fetchWordnik(word, hideEmptyPages, max).catch(error => (wordnikData = error))
    } else {
      // Source is datamuse
      let definitions = await fetchDefintionsDatamuse(word, loop).catch(error => (definitions = error))
      this.setState({ definitions: definitions })
      definitionData = { definitions: definitions }

      if (settings.synonymsVisible) {
        var synonyms = await fetchSynonyms(word, hideEmptyPages, max).catch(error => (synonyms = error))
      } else { synonyms = [] }

      if (settings.similarVisible) {
        var similar = await fetchSimilar(word, hideEmptyPages, max).catch(error => (similar = error))
      } else { similar = [] }

      if (settings.rhymesVisible) {
        var rhymes = await fetchRhymes(word, hideEmptyPages, max).catch(error => (rhymes = error))
      } else { rhymes = [] }

      if (settings.antonymsVisible) {
        var antonyms = await fetchAntonyms(word, hideEmptyPages, max).catch(error => (antonyms = error))
      } else { antonyms = [] }
    }

    // Retrieves the audio stream URI
    let audioUri = await fetchAudioUri(word).catch(error => (audioUri = error))
    this.callbacks.audioUri = audioUri

    if (settings.anagramsVisible) {
      // Retrieves anagrams from the anagramica API
      var anagrams = await fetchAnagrams(word, hideEmptyPages).catch(error => (anagrams = error))
    } else { anagrams = [] }

    if (settings.examplesVisible) {
      // Retrieves examples from the wordnik API
      var examples = await fetchExamples(word, hideEmptyPages).catch(error => (examples = error))
    } else { examples = [] }

    let otherWordData = {
      similar: wordnikData ? wordnikData.similar : similar,
      rhymes: wordnikData ? wordnikData.rhymes : rhymes,
      synonyms: wordnikData ? wordnikData.synonyms : synonyms,
      antonyms: wordnikData ? wordnikData.antonyms : antonyms,
      examples: examples,
      anagrams: anagrams
    }

    this.setState(otherWordData)

    if (settings.offlineCache && definitions !== 'No Data' && checkNetwork) {
      let wordData = Object.assign(definitionData, otherWordData)
      await AsyncStorage.setItem(word, JSON.stringify(wordData))
    }

    // Flashes scroll indicator to givs an indication of more horizontal content
    this.scrollViewRef.scrollTo({x: 0, y: 0, animated: true})
  }

  async makeRemoteRequestWordnik (page) {
    /** When user scrolls to the end of any of the lists where
    * ... the data is recieved from the wordnik API **/
    // Only called if (this.sourceIsWordnik === true)
    let word = this.callbacks.lowercaseWord
    let hideEmptyPages = this.callbacks.settings.hideEmptyPages
    let max = Math.floor((this.callbacks.height / 45) * page)

    let wordnikData = await fetchWordnik(word, hideEmptyPages, max)
    this.setState({
      similar: wordnikData.similar,
      rhymes: wordnikData.rhymes,
      synonyms: wordnikData.synonyms,
      antonyms: wordnikData.antonyms
    })
  }

  synonymList () {
    let onePageOfData = this.callbacks.onePageOfData
    let synonyms = this.state.synonyms
    let height = this.callbacks.height
    let width = this.callbacks.width
    let overrides = searchResultOverrides(
      false, height, width, this.callbacks.pagingEnabled)
    let listOverride = overrides.listOverride
    let widthOverride = overrides.widthOverride
    let settings = this.callbacks.settings

    if (settings.source) {
      var request = () => this.makeRemoteRequestWordnik(synonyms.length / onePageOfData + 1)
    } else {
      let page = synonyms.length / onePageOfData + 1
      let that = this
      request = async function () {
        let word = that.callbacks.lowercaseWord
        let hideEmptyPages = that.callbacks.settings.hideEmptyPages
        let max = Math.floor((that.callbacks.height / 45) * page)

        let synonyms = await fetchSynonyms(word, hideEmptyPages, max)
        if (synonyms !== []) {
          that.setState({ synonyms: synonyms })
        }
      }
    }

    return (
      <View style={widthOverride}>
        <FlatList
          style={listOverride}
          data={synonyms}
          renderItem={({ item }) => (
            <ListItem
              onPress={() => onPressItemSearchResultPage(
                item.word || item,
                settings,
                height,
                width
              )}
              onPressRightIcon={() => onPressItemSearchResultPage(
                item.word || item,
                settings,
                height,
                width
              )}
              titleNumberOfLines={1}
              onLongPress={() => copyToClipboard(item.word || item)}
              title={item.word || item} />
          )}
          initialNumToRender={onePageOfData}
          enableEmptySections
          onEndReached={request}
          onEndReachedThreshold={0.1}
          ListFooterComponent={footer}
          ListHeaderComponent={synonymHeader(this.callbacks.word)}
          ListEmptyComponent={listEmptyView}
          keyExtractor={(item, index) => index} />
      </View>
    )
  }

  antonymList () {
    let onePageOfData = this.callbacks.onePageOfData
    let antonyms = this.state.antonyms
    let height = this.callbacks.height
    let width = this.callbacks.width
    let overrides = searchResultOverrides(
      false, height, width, this.callbacks.pagingEnabled)
    let listOverride = overrides.listOverride
    let widthOverride = overrides.widthOverride
    let settings = this.callbacks.settings

    if (settings.sourceIsWordnik) {
      var request = () => this.makeRemoteRequestWordnik(antonyms.length / onePageOfData + 1)
    } else {
      let page = antonyms.length / onePageOfData + 1
      let that = this
      request = async function () {
        let word = that.callbacks.lowercaseWord
        let hideEmptyPages = that.callbacks.settings.hideEmptyPages
        let max = Math.floor((that.callbacks.height / 45) * page)

        let antonyms = await fetchAntonyms(word, hideEmptyPages, max)
        if (antonyms !== []) {
          that.setState({ antonyms: antonyms })
        }
      }
    }

    return (
      <View style={widthOverride}>
        <FlatList
          style={listOverride}
          data={antonyms}
          renderItem={({ item }) => (
            <ListItem
              onPress={() => onPressItemSearchResultPage(
                item.word || item,
                settings,
                height,
                width
              )}
              onPressRightIcon={() => onPressItemSearchResultPage(
                item.word || item,
                settings,
                height,
                width
              )}
              onLongPress={() => copyToClipboard(item.word || item)}
              titleNumberOfLines={1}
              title={item.word || item} />
          )}
          initialNumToRender={onePageOfData}
          onEndReached={request}
          onEndReachedThreshold={0.1}
          enableEmptySections
          ListFooterComponent={footer}
          ListHeaderComponent={antonymHeader(this.callbacks.word)}
          ListEmptyComponent={listEmptyView}
          keyExtractor={(item, index) => index} />
      </View>
    )
  }

  rhymeList () {
    let onePageOfData = this.callbacks.onePageOfData
    let rhymes = this.state.rhymes
    let height = this.callbacks.height
    let width = this.callbacks.width
    let overrides = searchResultOverrides(
      false, height, width, this.callbacks.pagingEnabled)
    let listOverride = overrides.listOverride
    let widthOverride = overrides.widthOverride
    let settings = this.callbacks.settings

    // Not sure how to handle these differing api requests if I
    // ... move the rhymeList to a seperate file
    if (settings.sourceIsWordnik) {
      var request = () => this.makeRemoteRequestWordnik(rhymes.length / onePageOfData + 1)
    } else {
      let page = rhymes.length / onePageOfData + 1
      let that = this
      request = async function () {
        let word = that.callbacks.lowercaseWord
        let hideEmptyPages = that.callbacks.settings.hideEmptyPages
        let max = Math.floor((that.callbacks.height / 45) * page)

        let rhymes = await fetchRhymes(word, hideEmptyPages, max)
        if (rhymes !== []) {
          that.setState({ rhymes: rhymes })
        }
      }
    }

    /**
    return (
      rhymeList(
        this.state.rhymes,
        this.overrides(),
        this.callbacks.settings,
        this.callbacks.height,
        this.callbacks.width,
        this.callbacks.word
      )
    )
    **/

    return (
      <View style={widthOverride}>
        <FlatList
          style={listOverride}
          data={rhymes}
          renderItem={({ item }) => (
            <ListItem
              onPress={() => onPressItemSearchResultPage(
                item.word || item,
                settings,
                height,
                width
              )}
              onPressRightIcon={() => onPressItemSearchResultPage(
                item.word || item,
                settings,
                height,
                width
              )}
              onLongPress={() => copyToClipboard(item.word || item)}
              titleNumberOfLines={1}
              title={item.word || item} />
          )}
          initialNumToRender={onePageOfData}
          onEndReached={request}
          onEndReachedThreshold={0.1}
          enableEmptySections
          ListFooterComponent={footer}
          ListHeaderComponent={rhymeHeader(this.callbacks.word)}
          ListEmptyComponent={listEmptyView}
          keyExtractor={(item, index) => index} />
      </View>
    )
  }

  similarList () {
    let onePageOfData = this.callbacks.onePageOfData
    let similar = this.state.similar
    let height = this.callbacks.height
    let width = this.callbacks.width
    let overrides = searchResultOverrides(
      false, height, width, this.callbacks.pagingEnabled)
    let listOverride = overrides.listOverride
    let widthOverride = overrides.widthOverride
    let settings = this.callbacks.settings

    if (settings.sourceIsWordnik) {
      var request = () => this.makeRemoteRequestWordnik(similar.length / onePageOfData + 1)
    } else {
      let page = similar.length / onePageOfData + 1
      let that = this
      request = async function () {
        let word = that.callbacks.lowercaseWord
        let hideEmptyPages = that.callbacks.settings.hideEmptyPages
        let max = Math.floor((that.callbacks.height / 45) * page)

        let similar = await fetchSimilar(word, hideEmptyPages, max)
        if (similar !== []) {
          that.setState({ similar: similar })
        }
      }
    }

    return (
      <View style={widthOverride}>
        <FlatList
          style={listOverride}
          data={similar}
          renderItem={({ item }) => (
            <ListItem
              onPress={() => onPressItemSearchResultPage(
                item.word || item,
                settings,
                height,
                width
              )}
              onPressRightIcon={() => onPressItemSearchResultPage(
                item.word || item,
                settings,
                height,
                width
              )}
              onLongPress={() => copyToClipboard(item.word || item)}
              titleNumberOfLines={1}
              title={item.word || item} />
          )}
          initialNumToRender={onePageOfData}
          onEndReached={request}
          onEndReachedThreshold={0.1}
          enableEmptySections
          ListFooterComponent={footer}
          ListHeaderComponent={similarHeader(this.callbacks.word)}
          ListEmptyComponent={listEmptyView}
          keyExtractor={(item, index) => index} />
      </View>
    )
  }

  render () {
    while (JSON.stringify(this.state.definitions) === '[]') {
      return (
        <View style={styles.wrapper}>
          {loading()}
        </View>
      )
    }

    return (
      <View style={[styles.wrapper, styles.paddingHorizontal0]}>
        <ScrollView pagingEnabled={this.callbacks.pagingEnabled} horizontal ref={(ref) => { this.scrollViewRef = ref }}>
          {definitionsList(
            this.state.definitions,
            searchResultOverrides(
              true,
              this.callbacks.height,
              this.callbacks.width,
              this.callbacks.pagingEnabled
            ),
            this.callbacks.word,
            this.callbacks.audioUri,
            this.callbacks.settings
          )}
          {renderIf(JSON.stringify(this.state.synonyms) !== '[]')(
            <View>
              {this.synonymList()}
            </View>
          )}
          {renderIf(JSON.stringify(this.state.antonyms) !== '[]')(
            <View>
              {this.antonymList()}
            </View>
          )}
          {renderIf(JSON.stringify(this.state.rhymes) !== '[]')(
            <View>
              {this.rhymeList()}
            </View>
          )}
          {renderIf(JSON.stringify(this.state.similar) !== '[]')(
            <View>
              {this.similarList()}
            </View>
          )}
          {renderIf(JSON.stringify(this.state.anagrams) !== '[]')(
            <View>
              {anagramsList(
                this.state.anagrams,
                searchResultOverrides(
                  false,
                  this.callbacks.height,
                  this.callbacks.width,
                  this.callbacks.pagingEnabled
                ),
                this.callbacks.settings,
                this.callbacks.height,
                this.callbacks.width,
                this.callbacks.word
              )}
            </View>
          )}
          {renderIf(JSON.stringify(this.state.examples) !== '[]')(
            <View>
              {examplesList(
                this.state.examples,
                searchResultOverrides(
                  true,
                  this.callbacks.height,
                  this.callbacks.width,
                  this.callbacks.pagingEnabled,
                  8
                ),
                this.callbacks.word,
                this.callbacks.settings
              )}
            </View>
          )}
        </ScrollView>
      </View>
    )
  }
}

export default SearchResult
