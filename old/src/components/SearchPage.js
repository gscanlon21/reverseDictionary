
import React, { Component } from 'react'
import {
  View,
  InteractionManager,
  Keyboard } from 'react-native'

/* Libraries */
import {
  initiateAnalyticsORAds,
  stopAnalyticsCollection }
  from './../config/FirebaseAnalyticsAdmob'
import renderIf from './../utilities/renderIf'
import { RaisedTextButton } from 'react-native-material-buttons'
import { TextField } from 'react-native-material-textfield'

/* Dev - styles */
import styles from './../styles/StyleWrapper'
import { searchOverrides } from './../styles/Overrides'
      // (ads, height, viewHeight)

/* Dev - settings */
import { loadAllSettings, loadHistory } from './../settings/Settings'

/* Dev - utilities */
import checkNetwork from './../utilities/Checks'

/* Dev - navigation */
import onSubmit from './../navigation/OnSubmit'
      // (word, height, width, settings, hist, viewHeight)
import onPressWOTDItem from './../navigation/OnPressWOTDItem'

/* Dev - apiRequests */
import fetchWOTD from './../apiRequests/FetchWOTD'
  // WOTD for Word Of The Day

/* Dev - views */
import historyList from './../views/HistoryList'
      // (history, listOverride, height, width, settings, wotd, similar)
import renderBannerAd from './../views/BannerAd'
      // (width)
import similarListRD from './../views/SimilarListRD'
      // (similar, listOverride, searchedWord, height, width, settings, hist, wotd)

class Search extends Component {
  constructor (props) {
    super(props)
    this.onFocus = this.onFocus.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.onChangeText = this.onChangeText.bind(this)
    this.wordRef = this.updateRef.bind(this, 'word')
    this.callbacks = {
      searchedWord: props.searchedWord || undefined,
      viewHeight: 108,
      height: undefined,
      width: undefined,
      history: [],
      autoFocus: !props.autoFocus
    }
    this.state = {
      hide: !props.hide,
      word: '',
      errors: {'word': null},
      similar: props.similar || [],
      wotd: undefined,
      settings: {historyDisabled: true}
    }
    // this.state.settings.historyDisabled should be
    // ... by default true to prevent the history list
    // ... from flashing open then closed when history is disabled
  }

  addListeners () {
    // Listener to hide the keyboard when the text-field loses focus
    Keyboard.addListener('keyboardDidHide', () => this.handleKeyboard())
  }

  componentWillReceiveProps () { // nextProps
    // Reloads settings and history when the user navigates back to this scene
    // this.props is used instead of nextProps so initial app open
    // ... doesn't trigger a refresh
    if (this.props.back) {
      this.loadSettings()
    }
  }

  async loadSettings () {
    /* Loads settings from local storage */
    let history = await loadHistory()
    this.callbacks.history = history

    let settings = await loadAllSettings()
    this.setState({ settings: settings })
  }

  async componentDidMount () {
    /* Called on app open */
    // Adds a listener to handle the keyboard when the text-field
    // ... is focused or not
    this.addListeners()

    // Loads history from local storage
    let history = await loadHistory()
    this.callbacks.history = history
    // Fetches stored settings and wotd
    this.setState({
      settings: await loadAllSettings()
    })
    this.setState({
      wotd: await fetchWOTD()
    })

    // So not everything loads during initial app start animations
    InteractionManager.runAfterInteractions(() => {
      // Check the user's network connection
      checkNetwork()
      // Allow settings to load first to see if usageStats or ads are enabled
      setTimeout(() => {
        initiateAnalyticsORAds(this.state.settings.ads)
      }, 1100)
    })
  }

  onLayout (event) {
    // Called whenever the layout changes (ie. app open or user rotates device)
    // Used to get width and height of screen
    if (this.callbacks.height) return // Layout was already called
    let {width, height} = event.nativeEvent.layout // Gets the width and height of device screen
    this.callbacks.height = height
    this.callbacks.width = width
  }

  viewLayout (event) {
    // Called whenever the layout changes (sa. app open or user rotates device)
    // Height of text input and button container
    let height = event.nativeEvent.layout.height
    // this.setState({viewHeight: height - 56})
    // Subtracting height of navbar (56px)
    this.callbacks.viewHeight = height - 56
  }

  componentWillUnmount () {
    // Removes listeners and stop analytics collection
    stopAnalyticsCollection()
    Keyboard.removeListener('keyboardDidHide', () => this.handleKeyboard())
  }

  handleKeyboard () {
    // Unfocuses from the text-field when the keyboard is closed
    try {
      this.word.blur()
    } catch (err) {
      // User closes keyboard before this.word is defined
    }
  }

  updateRef (name, ref) {
    this[name] = ref
  }

  onFocus () {
    // User focuses on text-field
    let { errors = {} } = this.state

    for (let name in errors) {
      let ref = this[name]

      if (ref && ref.isFocused()) {
        delete errors[name]
      }
    }

    if (!this.state.settings.historyDisabled) {
      this.setState({ errors, hide: true })
    } else {
      this.setState({ errors })
    }
  }

  onChangeText (text) {
    // User changes text in text-field
    ['word'].map((name) => ({ name, ref: this[name] })).forEach(({ name, ref }) => {
      if (ref.isFocused()) {
        this.setState({ [name]: text })
      }
    })
  }

  onSubmit () {
    // When user presses the submit button to search for a word
    let word = this.state.word

    // Checks for a valid word
    if (!word || word === '') {
      this.setState({errors: {'word': 'Should not be empty'}})
      return
    } else if (!word.match(/\b\w+\b/g)) {
      this.setState({errors: {'word': 'Invalid Word'}})
      return
    }

    // Unfocuses the text-field
    this.word.blur()

    onSubmit(
      word,
      this.callbacks.height,
      this.callbacks.width,
      this.state.settings
    )
  }

  onPressWOTDButton () {
    onPressWOTDItem(
      this.state.wotd,
      this.callbacks.height,
      this.callbacks.width,
      this.state.settings
    )
  }

  render () {
    let errors = this.state.errors
    let word = this.state.word
    let hide = this.state.hide
    let settings = this.state.settings
    let historyDisabled = settings.historyDisabled

    return (
      <View style={styles.wrapper} onLayout={(event) => this.onLayout(event)}>
        <View onLayout={(event) => this.viewLayout(event)}>
          <TextField
            autoFocus={this.callbacks.autoFocus}
            selectTextOnFocus
            ref={this.wordRef}
            value={word}
            autoCorrect={false}
            enablesReturnKeyAutomatically
            onFocus={this.onFocus}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmit}
            returnKeyType='go'
            label='Word or Phrase'
            error={errors.word}
          />
          <RaisedTextButton
            style={styles.button}
            onPress={Keyboard.dismiss && this.onSubmit}
            color={'#0091EA'}
            title='submit'
            titleColor='white'
          />
          {renderIf(this.state.wotd)(
            <RaisedTextButton
              style={styles.wotdButton}
              onPress={() => this.onPressWOTDButton() && Keyboard.dismiss}
              title='Word of the day'
            />
          )}
        </View>
        {renderIf(!hide)(
          <View style={styles.listWrapper}>{
            similarListRD(
              this.state.similar,
              searchOverrides(
                settings.ads,
                this.callbacks.height,
                this.callbacks.viewHeight
              ),
              this.callbacks.searchedWord,
              this.callbacks.height,
              this.callbacks.width,
              settings,
              this.callbacks.history
            )}
          </View>
        )}
        {renderIf(hide && !historyDisabled)(
          <View style={styles.listWrapper}>{
            historyList(
              this.callbacks.history,
              searchOverrides(
                settings.ads,
                this.callbacks.height,
                this.callbacks.viewHeight
              ),
              this.callbacks.height,
              this.callbacks.width,
              settings,
              this.state.similar
            )}
          </View>
        )}
      </View>
    )
  }
}

export default Search
