
import { Linking } from 'react-native'

function datamuseAPILink () {
  // Open link to the datamuse API
  Linking.openURL('https://www.datamuse.com/api/')
}

function sourceCodeLink () {
  // Open link to the datamuse API
  Linking.openURL('https://www.bitbucket.org/hofuchi/reversedictionary/overview/')
}

function anagramicaLink () {
  // Opens link to the anagramica API
  Linking.openURL('http://www.anagramica.com/')
}

function wordnikAPILink () {
  // Opens link to the wordnik API
  Linking.openURL('https://www.wordnik.com/')
}

export { datamuseAPILink, anagramicaLink, wordnikAPILink, sourceCodeLink }
