
import { Actions } from 'react-native-router-flux'

function backAndroid () {
  // Return to previous screen
  Actions.pop()
  // Needed so BackHandler knows that you are overriding the default
  // ... action and that it should not close the app
  return true
}

export default backAndroid
