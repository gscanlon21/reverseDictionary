
function handleErrors (response) {
  // Handles http request errors
  if (!response.ok) {
    throw Error(response.statusText)
  }
  return response
}

export default handleErrors
