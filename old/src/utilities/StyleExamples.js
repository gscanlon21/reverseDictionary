
import React from 'react'
import HTMLView from 'react-native-htmlview'

function styleExamples (itemText, searchedWord) {
  // Styles the example sentences
  let re = new RegExp('\\s("|\')*' + searchedWord + '\\b("|\')*', 'ig') // hello

  let htmlContent = itemText.replace(re, ' <b>' + searchedWord + '</b>').replace(/\s\b_/g, ' <i>').replace(/_\b/g, '</i>').replace(/\s\*\b/g, ' <b>').replace(/\b\*/g, '</b>').replace(/([\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2694-\u2697]|\uD83E[\uDD10-\uDD5D])/g, '')

  return (
    <HTMLView value={'<p>' + htmlContent + '</p>'} />
  )
}

export default styleExamples
