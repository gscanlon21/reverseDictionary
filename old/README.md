# About #

This Android project aims to consolidate the: reverse dictionary search feature, synonyms, antonyms, definitions, rhymes, anagrams, examples, similar words, and more -- into one concise application.

* Version: 1.5.6
* [Download on the Google Play Store](https://play.google.com/store/apps/details?id=com.gmail.gscanlon21.reversedictionary)
* [Download on the iTunes Store](https://itunes.apple.com/us/app/reverse-dictionary-and-thesaurus/id1273837413?mt=8)

* Features:
    - Search for a word by its definition
    - View a word's definition
    - View a word's rhymes
    - View a word's synonyms
    - View a word's antonyms
    - View a word's anagrams
    - View similar words
    - View example sentences for a word
    - Listen to a word's pronunciation
    - Relish the beautiful word of the day
    - Long press to copy a word or definition
    - Search history
    - See the Scrabble® score

### TODO ###

* More and better tests
    - Mock `react-native-firebase`
    - Implement more sophisticated and diverse tests
    - Increase code coverage
<br>
* iOS
    - Alternative to ToastAndroid needs to be implemented
<br>
* General
    - WOTD reloads on after the calling reverse dictionary feature

<br>

### Setup ###

* Setup [react-native](https://facebook.github.io/react-native/releases/0.47/docs/getting-started.html)
* Clone the project
```
$ npm install
$ react-native run-android
```

* Configuration
    - When testing the debug version for android make sure to remove:
        ```
        tools:node="remove"
        ```
    - from line 11 in android/app/src/main/AndroidManifest.xml:
        ```
        <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" tools:node="remove"/>
        ```

### Contributions and Guidelines ###

* Code should adhere to [Javascript Standard Style](https://standardjs.com/)

### Data Sources ###

* [The datamuse API](https://www.datamuse.com/api/)
* [The wordnik API](https://www.wordnik.com/)
* [The anagramica API](http://www.anagramica.com/api)

* Built using [react-native](https://facebook.github.io/react-native/)

### Who do I talk to? ###

Graham Scanlon - gscanlon21@gmail.com

### LICENSE ###

MIT

### Release Notes ###

* VC 67
    - Caching Data for offline access
    - New setting to switch between Words with Friends and Scrabble® word score
* VC 66
    - Style changes
    - Removed unused permission
* VC 65
    - Bug fixes: missing data, display name incorrect
* VC 62
    - Consolidating styles to make them easier to manage
    - Reworked how ads are arranged (they're now part of the <FlatList> footer)
    - Minor style adjusts
    - Filtered out irrelevant data (words of length 1 or 2)
    - History is longer and better at keeping track of what you last looked at
* VC 61
    - Ported to iOS
    + Bugs Fixed:
        + iOS
            - ToastAndroid is not defined
                - Implemented platform specific code
            - Anagrams, examples, audio and WOTD don't show up
                - Apple by default blocks insecure HTTP requests
        <br>
        + Android
            - FetchUnknownWord error message is skipped over from not importing ToastAndroid
* VC 59-60
    - Fixed a bug causing settings not to apply when navigating back to the search page
    - Slight optimizations for setState
    - Partial support for iOS
    - Missing audio pronunciation has been fixed
    - Added support for a wildcard when searching: `water*on` or `* pie`
* VC 58
    - Fixed bug where retrieving data for the audio with no network connection would cause the app to crash
    - Horizontal scroll bar is flashed when the content loads to let users know there's scroll-able content
* VC 57
    - Added some snapshot tests for the main components
    - Added features/API tests
    - Reworked app structure
    - Optimized use of setState() as well as eliminated parts of the state
    - Removed a good amount of:
        + Code duplication
        + Unnecessary code
        + Bugs with settings and and history caused by the restructuring
    - Formatted code to be easier to read with more comments
* VC 56
    - Reduced use of component state slightly
    - Added more comments
    - Combined similar code
    - Fixed math float error causing API request to fail
    - Minor bug and code improvements
* VC 54-55
    - Setting changes no longer require Restart
    - Reduced use of component state slightly
    - Anagrams page should now hide on empty data
    - Preliminary support for http status codes that aren't ok
    - Minor bug and code improvements
    - reverse-dictionary list should correctly render the header
