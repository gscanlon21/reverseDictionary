import React from 'react'
import { shallow } from 'enzyme'
import About from './../src/components/AboutPage'

describe('renders correctly', () => {
  it('renders as expected', () => {
    const wrapper = shallow(
      <About />
    )
    expect(wrapper).toMatchSnapshot()
  })
})
