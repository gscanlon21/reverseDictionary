
import React from 'react'
import { shallow } from 'enzyme'
import backAndroid from './../src/utilities/BackAndroid'
import checkNetwork from './../src/utilities/Checks'
import copyToClipboard from './../src/utilities/CopyToClipboard'
import handleErrors from './../src/utilities/HandleFetchErrors'
import styleExamples from './../src/utilities/StyleExamples'
jest.mock('react-native', () => {
  return {
    Clipboard: {
      setString: jest.fn()
    },
    ToastAndroid: {
      showWithGravity: jest.fn()
    }
  }
})
jest.mock('react-native-router-flux', () => {
  return {
    create: {
      create: jest.fn()
    },
    Actions: {
      pop: jest.fn()
    }
  }
})
jest.mock('react-native-htmlview', () => {
  return {
    get: {
      get: jest.fn()
    }
  }
})

describe('renders correctly', () => {
  it('should render backAndroid as expected', () => {
    const wrapper = shallow(<backAndroid />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render checkNetwork as expected', () => {
    const wrapper = shallow(<checkNetwork />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render copyToClipboard as expected', () => {
    const wrapper = shallow(<copyToClipboard />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render handleErrors as expected', () => {
    const wrapper = shallow(<handleErrors />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render styleExamples as expected', () => {
    const wrapper = shallow(<styleExamples />)
    expect(wrapper).toMatchSnapshot()
  })
})

describe('returns correct value', () => {
  it('should return the correct bool (true) for backAndroid', () => {
    expect(backAndroid()).toBe(true)
  })
})
