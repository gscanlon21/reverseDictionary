
import React from 'react'
import { shallow } from 'enzyme'
import fetchAnagrams from './../src/apiRequests/FetchAnagrams'
import fetchAntonyms from './../src/apiRequests/FetchAntonyms'
import fetchAudioUri from './../src/apiRequests/FetchAudioUri'
import fetchDefinitionsDatamuse from './../src/apiRequests/FetchDefinitionsDatamuse'
import fetchDefinitionsWordnik from './../src/apiRequests/FetchDefinitionsWordnik'
import fetchExamples from './../src/apiRequests/FetchExamples'
import fetchRhymes from './../src/apiRequests/FetchRhymes'
import fetchSimilar from './../src/apiRequests/FetchSimilar'
import fetchSynonyms from './../src/apiRequests/FetchSynonyms'
import fetchWordnik from './../src/apiRequests/FetchWordnik'
import fetchWOTD from './../src/apiRequests/FetchWOTD'
import reverseDictionaryRequest from './../src/apiRequests/ReverseDictionaryRequest'

describe('renders correctly', () => {
  it('should render fetchAnagrams as expected', () => {
    const wrapper = shallow(<fetchAnagrams />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchAntonyms as expected', () => {
    const wrapper = shallow(<fetchAntonyms />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchAudioUri as expected', () => {
    const wrapper = shallow(<fetchAudioUri />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchDefinitionsDatamuse as expected', () => {
    const wrapper = shallow(<fetchDefinitionsDatamuse />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchDefinitionsWordnik as expected', () => {
    const wrapper = shallow(<fetchDefinitionsWordnik />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchExamples as expected', () => {
    const wrapper = shallow(<fetchExamples />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchRhymes as expected', () => {
    const wrapper = shallow(<fetchRhymes />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchSimilar as expected', () => {
    const wrapper = shallow(<fetchSimilar />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchSynonyms as expected', () => {
    const wrapper = shallow(<fetchSynonyms />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchWordnik as expected', () => {
    const wrapper = shallow(<fetchWordnik />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render fetchWOTD as expected', () => {
    const wrapper = shallow(<fetchWOTD />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render reverseDictionaryRequest as expected', () => {
    const wrapper = shallow(<reverseDictionaryRequest />)
    expect(wrapper).toMatchSnapshot()
  })
})

describe('returns correct value', () => {
  it('should return the correct fetchAnagrams', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchAnagramsResponse))
    expect(await fetchAnagrams()).toMatchObject(fetchAnagramsResponse.all)
  })

  it('should return the correct fetchAntonyms', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchAntonymsResponse))
    expect(await fetchAntonyms()).toMatchObject(fetchAntonymsResponse)
  })

  it('should return the correct fetchAudioUri', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchAudioUriResponse))
    expect(await fetchAudioUri()).toBe(fetchAudioUriResponse[0].fileUrl)
  })

  it('should return the correct fetchDefinitionsDatamuse', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchDefinitionsDatamuseResponse))
    expect(await fetchDefinitionsDatamuse()).toMatchObject(fetchDefinitionsDatamuseResponse[0].defs)
  })

  it('should return the correct fetchDefinitionsWordnik', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchDefinitionsWordnikResponse))
    expect(await fetchDefinitionsWordnik()).toMatchObject(fetchDefinitionsWordnikResponse)
  })

  it('should return the correct fetchExamples', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchExamplesResponse))
    expect(await fetchExamples()).toMatchObject(fetchExamplesResponse.examples)
  })

  it('should return the correct fetchRhymes', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchRhymesResponse))
    expect(await fetchRhymes()).toMatchObject(fetchRhymesResponse)
  })

  it('should return the correct fetchSimilar', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchSimilarResponse))
    expect(await fetchSimilar()).toMatchObject(fetchSimilarResponse)
  })

  it('should return the correct fetchSynonyms', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchSynonymsResponse))
    expect(await fetchSynonyms()).toMatchObject(fetchSynonymsResponse)
  })

  it('should return the correct fetchWordnik', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchWordnikResponse))
    for (let i = 0; i < fetchWordnikResponse.length; i++) {
      if (JSON.stringify(fetchWordnikResponse[i].relationshipType) === '"synonym"') {
        var syn = fetchWordnikResponse[i].words
      } else if (JSON.stringify(fetchWordnikResponse[i].relationshipType) === '"antonym"') {
        var ant = fetchWordnikResponse[i].words
      } else if (JSON.stringify(fetchWordnikResponse[i].relationshipType) === '"rhyme"') {
        var rhy = fetchWordnikResponse[i].words
      } else if (JSON.stringify(fetchWordnikResponse[i].relationshipType) === '"hypernym"') {
        var hyp = fetchWordnikResponse[i].words
      }
    }
    expect(await fetchWordnik()).toMatchObject({
      synonyms: syn,
      antonyms: ant,
      rhymes: rhy,
      similar: hyp
    })
  })

  it('should return the correct fetchWOTD', async () => {
    fetch.mockResponseOnce(JSON.stringify(fetchWOTDResponse))
    expect(await fetchWOTD()).toBe(fetchWOTDResponse.word)
  })

  it('should return the correct reverseDictionaryRequest', async () => {
    fetch.mockResponseOnce(JSON.stringify(reverseDictionaryRequestResponse))
    expect(await reverseDictionaryRequest()).toBe(reverseDictionaryRequestResponse)
  })
})

const fetchAnagramsResponse = {
  'all': [
    'hello',
    'hell',
    'hole',
    'hoe',
    'eh',
    'he',
    'ell',
    'ho',
    'oh',
    'lo',
    'e',
    'h',
    'l',
    'o'
  ]
}

const fetchAntonymsResponse = [{'word': 'back', 'score': 5181}, {'word': 'reverse', 'score': 1847}, {'word': 'aft', 'score': 707}, {'word': 'backward', 'score': 692}, {'word': 'abaft', 'score': 527}, {'word': 'backwards', 'score': 487}, {'word': 'astern', 'score': 379}, {'word': 'rearward', 'score': 171}, {'word': 'rearwards', 'score': 59}]

const fetchAudioUriResponse = [{'commentCount': 0, 'createdBy': 'ahd', 'createdAt': '2009-03-15T15:32:16.000+0000', 'id': 23095, 'word': 'forward', 'duration': 1.25, 'audioType': 'pronunciation', 'attributionText': 'from The American Heritage® Dictionary of the English Language, 4th Edition', 'fileUrl': 'http://api.wordnik.com/v4/audioFile.mp3/3b240d85a0fe35d9a307e70d53f4c58fb3b755e0da1e2a0ee400f11fc1d93c1c'}, {'commentCount': 0, 'description': 'Don’t drop the first /r/ and say FOH-wurd. It should be FOR-word. - The Orthoepist', 'createdBy': 'chelster', 'createdAt': '2010-07-16T23:30:46.000+0000', 'id': 75925, 'word': 'forward', 'duration': 5.34, 'audioType': 'pronunciation', 'fileUrl': 'http://api.wordnik.com/v4/audioFile.mp3/b70622b3daa2702014b5d9167c672e025b8f39b1dbfd385da324e1743d959445'}]

const fetchDefinitionsDatamuseResponse = [{'word': 'forward', 'score': 73682, 'defs': ['n\ta position on a basketball team', 'n\tthe person who plays the position of forward on a basketball team', 'v\tsend or ship onward from an intermediate post or station in transit', 'adj\tmoving toward a position ahead', 'adj\tsituated in the front', 'adj\tat or near or directed toward the front', 'adj\tof the transmission gear causing forward movement in a motor vehicle', 'adj\tused of temperament or behavior; lacking restraint or modesty', 'adj\tsituated at or toward the front', 'adj\tmoving forward', 'adv\tat or to or toward the front', 'adv\ttoward the future; forward in time', 'adv\tforward in time or order or degree', 'adv\tnear or toward the bow of a ship or cockpit of a plane', 'adv\tin a forward direction']}]

const fetchDefinitionsWordnikResponse = [{'textProns': [], 'sourceDictionary': 'ahd-legacy', 'exampleUses': [], 'relatedWords': [], 'labels': [], 'citations': [], 'word': 'forward', 'sequence': '0', 'partOfSpeech': 'adjective', 'attributionText': 'from The American Heritage® Dictionary of the English Language, 4th Edition', 'text': 'At, near, or belonging to the front or forepart; fore:  the forward section of the aircraft. ', 'score': 0.0}, {'textProns': [], 'sourceDictionary': 'ahd-legacy', 'exampleUses': [], 'relatedWords': [], 'labels': [], 'citations': [], 'word': 'forward', 'sequence': '1', 'partOfSpeech': 'adjective', 'attributionText': 'from The American Heritage® Dictionary of the English Language, 4th Edition', 'text': 'Located ahead or in advance:  kept her eye on the forward horizon. ', 'score': 0.0}, {'textProns': [], 'sourceDictionary': 'ahd-legacy', 'exampleUses': [], 'relatedWords': [], 'labels': [], 'citations': [], 'word': 'forward', 'sequence': '2', 'partOfSpeech': 'adjective', 'attributionText': 'from The American Heritage® Dictionary of the English Language, 4th Edition', 'text': 'Going, tending, or moving toward a position in front:  a forward plunge down a flight of stairs. ', 'score': 0.0}, {'textProns': [], 'sourceDictionary': 'ahd-legacy', 'exampleUses': [], 'relatedWords': [], 'labels': [], 'citations': [], 'word': 'forward', 'sequence': '3', 'partOfSpeech': 'adjective', 'attributionText': 'from The American Heritage® Dictionary of the English Language, 4th Edition', 'text': "Sports   Advancing toward an opponent's goal.", 'score': 0.0}, {'textProns': [], 'sourceDictionary': 'ahd-legacy', 'exampleUses': [], 'relatedWords': [], 'labels': [], 'citations': [], 'word': 'forward', 'sequence': '4', 'partOfSpeech': 'adjective', 'attributionText': 'from The American Heritage® Dictionary of the English Language, 4th Edition', 'text': 'Moving in a prescribed direction or order for normal use:  forward rolling of the cassette tape. ', 'score': 0.0}, {'textProns': [], 'sourceDictionary': 'ahd-legacy', 'exampleUses': [], 'relatedWords': [], 'labels': [], 'citations': [], 'word': 'forward', 'sequence': '5', 'partOfSpeech': 'adjective', 'attributionText': 'from The American Heritage® Dictionary of the English Language, 4th Edition', 'text': 'Ardently inclined; eager.', 'score': 0.0}]

const fetchExamplesResponse = {'examples': [{'provider': {'name': 'spinner', 'id': 712}, 'year': 2009, 'rating': 8572.236, 'url': 'http://api.wordnik.com/v4/mid/485f58a1b2e65304d143517b0ab502d04cccaf9cff6b408272975f30ad458958', 'word': 'forward', 'text': 'We now look forward to the latter part of the present campaign for the World Cup in 2010 and certainly look ­forward to the', 'documentId': 11040025, 'exampleId': 104473475, 'title': 'Football news, match reports and fixtures | guardian.co.uk'}, {'provider': {'name': 'voa', 'id': 718}, 'year': 2011, 'rating': 764.0, 'url': 'http://api.wordnik.com/v4/mid/aa5972080ec1ea26c826d6a97866ec89abdbb144ccec83dd4ff8e6fb96676eae', 'word': 'forward', 'text': "A boy, they believe, will be able to look after them in old age, he will carry the name forward of the family - and that's very, very important to Indian families.", 'documentId': 32541584, 'exampleId': 613901866, 'title': 'Researchers: Indians Increasingly Use Abortion to Ensure Male Child'}, {'provider': {'name': 'simonschuster', 'id': 722}, 'year': 2011, 'rating': 764.0, 'url': 'http://api.wordnik.com/v4/mid/e139ad646badf00e045b5b1bd66facebbccfcdef3f8edc5d268cac0abc8fa3a8', 'word': 'forward', 'text': 'When the Calcutta intelligence chief suggested someone go on an “errand-boy visit” to check out the neighboring MO operation in Kandy, Betty had immediately put her name forward in hopes of seeing her friend again.', 'documentId': 32561756, 'exampleId': 715951518, 'title': 'A Covert Affair'}, {'provider': {'name': 'wsj', 'id': 707}, 'year': 2012, 'rating': 764.0, 'url': 'http://api.wordnik.com/v4/mid/424e0efbdac0ea402c71b649f0b0488e0648782aa1a9a1528ac6db44e53539e1', 'word': 'forward', 'text': 'When he was deputy mayor of the eastern port of Xiamen, he put his name forward as a candidate to be promoted for mayor—without the approval of the party leadership, according to people who knew him at the time.', 'documentId': 32831176, 'exampleId': 647808937, 'title': "Early Hardship Shaped Xi's World View"}, {'provider': {'name': 'guardian', 'id': 709}, 'year': 2011, 'rating': 764.0, 'url': 'http://api.wordnik.com/v4/mid/606cbbd10a8cb796d818220d047b8961a481af8b8acae3d58cd85ec753139efc', 'word': 'forward', 'text': "While Grant may rue the missed opportunity, perhaps he should think of putting his name forward for a different gong – Fleet Street's scoop of the year.", 'documentId': 32456017, 'exampleId': 565325813, 'title': 'Rebekah Brooks in firing line as phone-hacking scandal refuses to go away'}, {'provider': {'name': 'wapo', 'id': 708}, 'year': 2011, 'rating': 764.0, 'url': 'http://api.wordnik.com/v4/mid/6408f6f661434bc16868f322f70285f415984744c470085ed5bd6430b879ebdd', 'word': 'forward', 'text': 'Glendening D, first successfully put her name forward for the job 10 years ago.', 'documentId': 32382465, 'exampleId': 543152191, 'title': "No objections raised as Judge O'Malley appears before Md. Senate panel"}]}

const fetchRhymesResponse = [{'word': 'straightforward', 'score': 1288, 'numSyllables': 3}, {'word': 'shoreward', 'score': 276, 'numSyllables': 2}, {'word': 'carryforward', 'score': 46, 'numSyllables': 4}, {'word': 'dorward', 'score': 6, 'numSyllables': 2}, {'word': 'norward', 'score': 2, 'numSyllables': 2}, {'word': 'before word', 'numSyllables': 3}, {'word': 'core word', 'numSyllables': 2}, {'word': 'explore word', 'numSyllables': 3}, {'word': 'for word', 'numSyllables': 2}, {'word': 'fore word', 'numSyllables': 2}, {'word': 'ignore word', 'numSyllables': 3}, {'word': 'more word', 'numSyllables': 2}, {'word': 'nor word', 'numSyllables': 2}, {'word': 'or word', 'numSyllables': 2}, {'word': 'store word', 'numSyllables': 2}, {'word': 'your word', 'numSyllables': 2}]

const fetchSimilarResponse = [{'word': 'aft', 'score': 1502}, {'word': 'backward', 'score': 1475}, {'word': 'superstructure', 'score': 1440}, {'word': 'fuselage', 'score': 1375}, {'word': 'bloc', 'score': 1361}, {'word': 'turret', 'score': 1325}, {'word': 'cockpit', 'score': 1318}, {'word': 'reverse', 'score': 1288}, {'word': 'deck', 'score': 1255}, {'word': 'rear', 'score': 1227}, {'word': 'bow', 'score': 1224}, {'word': 'guns', 'score': 1211}, {'word': 'firing', 'score': 1209}, {'word': 'motion', 'score': 1204}, {'word': 'gun', 'score': 1199}, {'word': 'nba', 'score': 1198}, {'word': 'goals', 'score': 1196}, {'word': 'nhl', 'score': 1190}, {'word': 'fitted', 'score': 1188}, {'word': 'bases', 'score': 1183}, {'word': 'deployed', 'score': 1182}, {'word': 'positions', 'score': 1180}]

const fetchWordnikResponse = [{'relationshipType': 'equivalent', 'words': ['progressive', 'headlong', 'headfirst', 'full-face', 'guardant', 'gardant', 'assuming', 'overfamiliar', 'wise', 'presumptuous', 'sassy', 'brash', 'overbold', 'impudent', 'cheeky', 'bumptious', 'fresh', 'nervy', 'impertinent', 'self-assertive']}, {'relationshipType': 'antonym', 'words': ['backward']}, {'relationshipType': 'verb-form', 'words': ['forwarded', 'forwarding', 'forwards']}, {'relationshipType': 'hypernym', 'words': ['send', 'transport', 'ship', 'position']}, {'relationshipType': 'cross-reference', 'words': ['fore', 'ready', 'bold', "to put one's best foot forward", 'forward glance', 'forward play', 'drawn forward', 'to bring forward', 'foreword']}, {'relationshipType': 'form', 'words': ['look forward to', 'look forward', 'fast forward', 'freight forwarder', 'forwarding address']}, {'relationshipType': 'synonym', 'words': ['agreement', 'covenant', 'promise', 'onward', 'progressively', 'ready', 'prompt', 'ardent', 'eager', 'earnest', 'bold', 'confident', 'transmit', 'onwards', 'on', 'along', 'forth', 'forthward', 'before', 'ahead']}, {'relationshipType': 'rhyme', 'words': ['norward', 'shoreward', 'straightforward']}, {'relationshipType': 'unknown', 'words': ['Memes']}, {'relationshipType': 'same-context', 'words': ['vertical', 'additional', 'upper', 'upward', 'main', 'fast', 'aft', 'then', 'tactical', 'make', 'forth', 'well', 'away', 'out', 'down']}]

const fetchWOTDResponse = {'id': 521305, 'word': 'adit', 'note': "The word 'adit' comes from a Latin word meaning \"entrance, access\".", 'publishDate': '2017-08-08T03:00:00.000+0000', 'contentProvider': {'name': 'wordnik', 'id': 711}, 'examples': [{'url': 'http://api.wordnik.com/v4/mid/103815a78ac9378fe5cf6268b17e30ecbcc8a156458fbab69c379967c9be465e', 'text': 'We’re standing beneath the adit of our long-desolate cave, proffering a sheaf of papers that you might consider a manuscript.', 'id': 293112589, 'title': 'What Kind of Young Writer Were You?'}, {'url': 'http://api.wordnik.com/v4/mid/047970b7f93283b617ad7fd4f7094a094ec97e2c0e785d65bf484eea4c34cbb4', 'text': 'A narrow and untrodden cavern at the bottom connects it with the outer sea; they could even then hear the mysterious thunder and gurgle of the surge in the subterranean adit, as it rolled huge boulders to and fro in darkness, and forced before it gusts of pent-up air.', 'id': 1091748855, 'title': 'Westward Ho!'}, {'url': 'http://api.wordnik.com/v4/mid/88c0ca95aa38290f169e0f0be6db10325159118f6a9d4614799ed216a4b00976', 'text': '"Shh!" said an older child, cocking his head and listening very hard, his eyes never moving from the cloud exuding from the adit.', 'id': 1035812902, 'title': "Dragon's Kin"}, {'url': 'http://api.wordnik.com/v4/mid/face2518e4a9e539a864233d4e34e3fe233bfc2c6cfd4d6fc064622f67fd19a5', 'text': 'After the last verse Taita turned and, with every eye fixed avidly upon him, strode back down the adit until he stood before the blue-grey wall of newly exposed rock at the end.', 'id': 1063770474, 'title': 'Warlock'}, {'url': 'http://api.wordnik.com/v4/mid/de0dc7214cf4ac8d1229048918e2ccf8ccb97dae1a9435fbd60f5ba765a39cff', 'text': "Then he'll be needed at the mine, when we've got the adit cleared.", 'id': 1077812199, 'title': 'The Rowan'}], 'definitions': [{'text': 'An almost horizontal entrance to a mine.', 'partOfSpeech': 'noun', 'source': 'ahd-legacy'}]}

const fetchSynonymsResponse = [{'word': 'smart', 'score': 3551}, {'word': 'first', 'score': 1623}, {'word': 'fresh', 'score': 1609}, {'word': 'forth', 'score': 1442}, {'word': 'second', 'score': 1426}, {'word': 'low', 'score': 1357}, {'word': 'presumptuous', 'score': 1332}, {'word': 'cheeky', 'score': 1266}, {'word': 'familiar', 'score': 1234}, {'word': 'brash', 'score': 1154}, {'word': 'ahead', 'score': 1152}, {'word': 'progressive', 'score': 1125}, {'word': 'impertinent', 'score': 1010}, {'word': 'impudent', 'score': 1001}, {'word': 'fore', 'score': 968}, {'word': 'bumptious', 'score': 626}, {'word': 'onward', 'score': 597}, {'word': 'saucy', 'score': 528}, {'word': 'onwards', 'score': 416}, {'word': 'headlong', 'score': 406}, {'word': 'nervy', 'score': 375}, {'word': 'assuming', 'score': 317}, {'word': 'assumptive', 'score': 313}, {'word': 'headfirst', 'score': 287}, {'word': 'forwards', 'score': 258}, {'word': 'overbold', 'score': 249}, {'word': 'advancing', 'score': 224}, {'word': 'forrader', 'score': 112}, {'word': 'send on', 'score': 94}, {'word': 'frontward', 'score': 85}, {'word': 'self-assertive', 'score': 76}, {'word': 'forrard', 'score': 69}, {'word': 'forrad', 'score': 68}, {'word': 'overfamiliar', 'score': 66}, {'word': 'frontwards', 'score': 43}, {'word': 'forward-moving', 'score': 35}, {'word': 'full-face', 'score': 35}]

const reverseDictionaryRequestResponse = '[{"word":"ahead","score":31030,"tags":["syn","adv"]},{"word":"forth","score":28061,"tags":["syn","adv"]},{"word":"fore","score":24037,"tags":["syn","n"]},{"word":"front","score":22919,"tags":["syn","n"]},{"word":"fresh","score":22499,"tags":["syn","adj"]},{"word":"progressive","score":21872,"tags":["syn","adj"]}]'
