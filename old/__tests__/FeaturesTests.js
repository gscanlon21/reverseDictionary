
import React from 'react'
import { shallow } from 'enzyme'
import playAudio from './../src/features/PlayAudio'
import scrabbleScore from './../src/features/ScrabbleScore'

describe('renders correctly', () => {
  it('should render playAudio as expected', () => {
    const wrapper = shallow(<playAudio />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render scrabbleScore as expected', () => {
    const wrapper = shallow(<scrabbleScore />)
    expect(wrapper).toMatchSnapshot()
  })
})

describe('returns correct value', () => {
  // All inputs are lowercased before passing to scrabbleScore
  it('should return the correct scrabbleScore', () => {
    expect(scrabbleScore('scrabble')).toBe(14)
  })
  it('should return the correct scrabbleScore', () => {
    expect(scrabbleScore('apple')).toBe(9)
  })
  it('should return the correct scrabbleScore', () => {
    expect(scrabbleScore('pie')).toBe(5)
  })
})
