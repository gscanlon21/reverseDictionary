
import React from 'react'
import { shallow } from 'enzyme'
import Settings from './../src/components/SettingsPage'

describe('renders correctly', () => {
  it('renders as expected', () => {
    const wrapper = shallow(
      <Settings />
    )
    expect(wrapper).toMatchSnapshot()
  })
})
