
import React from 'react'
import { shallow } from 'enzyme'
// import { adRequest, Banner } from './../src/constants/Ads'
import {
  datamuseAPILink,
  anagramicaLink,
  wordnikAPILink,
  sourceCodeLink } from './../src/constants/ExternalLinks'
import {
  refreshOnBack,
  homeButton,
  settingsButton,
  reducerCreate,
  aboutButton } from './../src/constants/MainConstants'

jest.mock('react-native-firebase', () => {
  return {
    initializeApp: jest.fn()
  }
})

// TypeError: Cannot read property 'admob' of undefined
describe('renders correctly', () => {
  xit('should render adRequest as expected', () => {
    const wrapper = shallow(<adRequest />)
    expect(wrapper).toMatchSnapshot()
  })

  xit('should render Banner as expected', () => {
    const wrapper = shallow(<Banner />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render datamuseAPILink as expected', () => {
    const wrapper = shallow(<datamuseAPILink />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render anagramicaLink as expected', () => {
    const wrapper = shallow(<anagramicaLink />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render wordnikAPILink as expected', () => {
    const wrapper = shallow(<wordnikAPILink />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render sourceCodeLink as expected', () => {
    const wrapper = shallow(<sourceCodeLink />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render reducerCreate as expected', () => {
    const wrapper = shallow(<reducerCreate />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render settingsButton as expected', () => {
    const wrapper = shallow(<settingsButton />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render homeButton as expected', () => {
    const wrapper = shallow(<homeButton />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render refreshOnBack as expected', () => {
    const wrapper = shallow(<refreshOnBack />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render aboutButton as expected', () => {
    const wrapper = shallow(<aboutButton />)
    expect(wrapper).toMatchSnapshot()
  })
})

/*
describe('returns correct value', () => {
  it('should return the correct scrabbleScore', () => {
      expect(scrabbleScore('scrabble')).toBe(14);
  });
})
*/
