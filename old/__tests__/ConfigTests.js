
import React from 'react'
import { shallow } from 'enzyme'
import firebase from './../src/config/Firebase'
import {
  initiateAnalyticsORAds,
  stopAnalyticsCollection } from './../src/config/FirebaseAnalyticsAdmob'

jest.mock('react-native-firebase', () => {
  return {
    initializeApp: jest.fn()
  }
})

describe('renders correctly', () => {
  it('should render firebase as expected', () => {
    const wrapper = shallow(<firebase />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render initiateAnalyticsORAds as expected', () => {
    const wrapper = shallow(<initiateAnalyticsORAds />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render stopAnalyticsCollection as expected', () => {
    const wrapper = shallow(<stopAnalyticsCollection />)
    expect(wrapper).toMatchSnapshot()
  })
})

/*
describe('returns correct value', () => {
  it('should return the correct scrabbleScore', () => {
      expect(scrabbleScore('scrabble')).toBe(14);
  });
})
*/
