
import React from 'react'
import { shallow } from 'enzyme'
import {
  onPressItemSearchResultPage,
  onPressItemSearchPage } from './../src/navigation/OnPressItem'
import onPressWOTDItem from './../src/navigation/OnPressWOTDItem'
import onSubmit from './../src/navigation/OnSubmit'

describe('renders correctly', () => {
  it('should render onPressItemSearchResultPage as expected', () => {
    const wrapper = shallow(<onPressItemSearchResultPage />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render onPressItemSearchPage as expected', () => {
    const wrapper = shallow(<onPressItemSearchPage />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render onPressWOTDItem as expected', () => {
    const wrapper = shallow(<onPressWOTDItem />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render onSubmit as expected', () => {
    const wrapper = shallow(<onSubmit />)
    expect(wrapper).toMatchSnapshot()
  })
})

/*
describe('returns correct value', () => {
  // TypeError: Cannot read property 'getStateForAction' of undefined
  // ... occurs when calling Actions.pop()
  // EDIT
  // Just needed to mock react-native-router-flux
  it('should return the correct bool (true) for backAndroid', () => {
      expect(backAndroid()).toBe(true);
  });
})
*/
